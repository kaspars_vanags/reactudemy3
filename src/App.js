import React, { useEffect, useReducer } from 'react';
import './App.css';
import Styles from './Main.module.css';

import Header from './components/Header';
import Body from './components/Body';

import BasketContext from './contex/basket-contex';


const basketListReducer = (lastState, action) => {

  if (action.type === "INITI_STORE") {
    return action.list;
  }

  if (action.type === "ADD_BASKET_ITEM") {

    localStorage.removeItem("storageBasketList");

    const itemExistIndex = lastState.findIndex(cartItem => cartItem.name === action.item.name);
    const existingCartItem = lastState[itemExistIndex];

    let updatedItem;
    let updatedItems; 

    if (existingCartItem) {
      updatedItem = {
        ...existingCartItem, amount: existingCartItem.amount + action.item.amount
      }

      updatedItems = [...lastState];
      updatedItems[itemExistIndex] = updatedItem;
    } else {
      updatedItems = lastState.concat(action.item);
    }

    localStorage.setItem("storageBasketList", JSON.stringify(updatedItems));
    return updatedItems;
  }

  if (action.type === "REMOVE_BASKET_ITEM") {

    localStorage.removeItem("storageBasketList");

    const itemExistIndex = lastState.findIndex(cartItem => cartItem.name === action.item.name);
    const existingCartItem = lastState[itemExistIndex];

    let updatedItems;
    let updatedItem;

    if (existingCartItem.amount === 1) {
      updatedItems = lastState.filter(item => item.name !== action.item.name);
    } else {
      updatedItem = {...existingCartItem, amount: existingCartItem.amount - action.item.amount};
      updatedItems = [...lastState];
      updatedItems[itemExistIndex] = updatedItem;
    }

    localStorage.setItem("storageBasketList", JSON.stringify(updatedItems));
    return updatedItems;
  }

  return [];
}

function App() {

  console.log("i was reloaded");
  const [basketList, dispatchBasketList] = useReducer(basketListReducer, []);

  useEffect(() => {

    const storedBasketItems = JSON.parse(localStorage.getItem("storageBasketList"));  

    if (storedBasketItems) {
      dispatchBasketList({type: "INITI_STORE", list: storedBasketItems})
    }

  }, []);

  const addItemToTheBasket = (newBasketItem) => {
    dispatchBasketList({type: "ADD_BASKET_ITEM", item: newBasketItem});
  }

  const removeItemFromTheBasket = (oldBasketItem) => {
    dispatchBasketList({type: "REMOVE_BASKET_ITEM", item: oldBasketItem});
  }

  return (
    <BasketContext.Provider value={{basket : basketList, addItem: addItemToTheBasket, removeItem: removeItemFromTheBasket}}>
      <Header></Header>
      <main className={Styles.background}>
        <Body></Body>
      </main>
    </BasketContext.Provider>
  );
}

export default App;
