import React from "react";
import ReactDOM from "react-dom"
import Styles from "./Modal.module.css";
import Cart from "../components/Cart";

const BackDrop = (props) => {
    return <div className={Styles.backdrop} onClick={props.closeModal}></div>;
}

const BasketModal = (props) => {
    return (
    <div className={Styles.modal}>
        <Cart closeModal={props.closeModal} basketList={props.basketItems}></Cart>
    </div>
    );
}

const Modal = (props) => {
    return <React.Fragment>
        {ReactDOM.createPortal(<BackDrop closeModal={props.closeModal}></BackDrop>, document.getElementById("backdrop-root"))}
        {ReactDOM.createPortal(<BasketModal closeModal={props.closeModal} basketItems={props.basketItems}></BasketModal>, document.getElementById("basket-modal-root"))}
    </React.Fragment>
}

export default Modal;