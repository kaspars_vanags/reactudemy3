import React, { useContext, useState, useRef } from 'react';
import Input from '../../UI/Input';
import Styles from './MealItemForm.module.css';

import BasketContext from '../../contex/basket-contex';

const MealItemForm = (props) => {

    const amountInputRef = useRef();
    const [formIsValid, setFormIsValid] = useState(true);

    const ctx = useContext(BasketContext);

    const addItem = (amount) => {
        ctx.addItem({name: props.itemName, price: props.price, amount: amount});
    }

    const submitMealItem = (event) => {
        event.preventDefault();

        const enteredAmount = amountInputRef.current.value;
        const enteredAmountNumber = +enteredAmount;

        if (enteredAmount.trim().length === 0 || enteredAmountNumber < 1 || enteredAmountNumber > 5) {
            setFormIsValid(false);
            return;
        }

        setFormIsValid(true);
        addItem(enteredAmountNumber);
        amountInputRef.current.value = 1;
    }

    return (
        <form className={Styles.form} onSubmit={submitMealItem}>
            <Input label="Amount" 
            ref={amountInputRef}
            input={{
                id: 'amount_' + props.id, 
                type: 'number', 
                //onChange: updateItemCount, 
                min:'1', 
                max: '5', 
                step: '1', 
                defaultValue: '1'
            }}  
            />
            <button type='submit'>+ Add</button>
            {!formIsValid && <p>Enter valid item amount!</p>}
        </form>
    )
}

export default MealItemForm;