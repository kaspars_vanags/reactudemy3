import React, { useContext, useEffect, useState } from 'react';
import CartIcon from './CartIcon';
import Styles from './HeaderCartButton.module.css';

import BasketContext from '../../contex/basket-contex';

const HeaderCartButton = (props) => {

    const [buttonAnimated, setButtonAnimated] = useState(false);
    const ctx = useContext(BasketContext);
    const { basket } = ctx;

    const cartItemCount = basket.reduce((current, item) => {return current + item.amount}, 0);

    const btnClasses = `${Styles.button} ${buttonAnimated ? Styles.bump : ''}`;

    useEffect(() => {
        
        if (basket.length === 0) {
            return;
        }

        setButtonAnimated(true);

        const timer = setTimeout(() => {
            setButtonAnimated(false);
        }, 300)

        return () => {
            clearTimeout(timer);
        };
    }, [basket])

    return (
    <div className={btnClasses} onClick={props.openModal}>
        <div className={Styles.icon}>
            <CartIcon></CartIcon>
        </div>
        <div>Your cart</div>
        <div className={Styles.badge}>{cartItemCount}</div>
    </div>
    );
}

export default HeaderCartButton;