import React from "react";
import Styles from "./MealItem.module.css";
import MealItemForm from "./MealItemForm";

const MealItem = (props) => {
    return (
    <li className={Styles.meal}>
        <div>
            <h3>{props.name}</h3>
            <p className={Styles.description}>{props.description}</p>
            <p className={Styles.price}>${props.price}</p>
        </div>
        <div>
            <MealItemForm id={props.id} price={props.price} itemName={props.name}></MealItemForm>
        </div>
    </li>
    );
}

export default MealItem;       