import Styles from './CartItem.module.css';
import BasketContext from '../contex/basket-contex';
import React, { useContext } from 'react';

const CartItem = (props) => {

  const ctx = useContext(BasketContext);
  const price = `$${props.price.toFixed(2)}`;


  const onRemove = () => {
    ctx.removeItem({name: props.name, price: props.price, amount: 1});
  }

  const onAdd = () => {
    ctx.addItem({name: props.name, price: props.price, amount: 1});
  }

  return (
    <li className={Styles['cart-item']}>
      <div>
        <h2>{props.name}</h2>
        <div className={Styles.summary}>
          <span className={Styles.price}>{price}</span>
          <span className={Styles.amount}>x {props.amount}</span>
        </div>
      </div>
      <div className={Styles.actions}>
        <button onClick={onRemove}>−</button>
        <button onClick={onAdd}>+</button>
      </div>
    </li>
  );
};

export default CartItem;