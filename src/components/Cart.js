import React from 'react';
import Styles from './Cart.module.css';

import CartItem from './CartItem';

const Cart = (props) => {

    let totalAmount = 0;

    if (props.basketList.length > 0) {
        totalAmount = (props.basketList.reduce((total, current) => 
            total + (current.price * current.amount)
        , 0)).toFixed(2);
    }

    return (
    <React.Fragment>
        {props.basketList.map((item) => {
            return <CartItem price={item.price} amount={item.amount} name={item.name}></CartItem>
        })}
        <div className={Styles.total}>
            <span>Total amount</span><span>${totalAmount}</span>
        </div>
        <div className={Styles.actions}>
            <button onClick={props.closeModal} className={Styles["button--alt"]}>Close</button><button className={Styles.button}>Order</button>
        </div>
    </React.Fragment>
    );
}

export default Cart;