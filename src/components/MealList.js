import React from "react";
import Styles from "./MealList.module.css";
import MealItem from "./partials/MealItem";
import Card from '../UI/Card';


const DUMMY_MEALS = [
  {
    id: 'm1',
    name: 'Sushi',
    description: 'Finest fish and veggies',
    price: 22.99,
  },
  {
    id: 'm2',
    name: 'Schnitzel',
    description: 'A german specialty!',
    price: 16.5,
  },
  {
    id: 'm3',
    name: 'Barbecue Burger',
    description: 'American, raw, meaty',
    price: 12.99,
  },
  {
    id: 'm4',
    name: 'Green Bowl',
    description: 'Healthy...and green...',
    price: 18.99,
  },
];

const MealList = () => {

  const mealsList = DUMMY_MEALS.map((e) => <MealItem key={e.id} id={e.id} name={e.name} description={e.description} price={e.price}></MealItem>)

  return (
      <section className={Styles.meals}>
          <Card>
            <ul className={Styles['meals-appear']}>
              {mealsList}
            </ul>
          </Card>
      </section>
  );
}

export default MealList;