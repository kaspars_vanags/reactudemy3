import  React, { Fragment } from 'react'
import MealsSummary from './partials/MealsSummary';
import MealList from './MealList';

const Body = () => {
    return <Fragment>
        <MealsSummary></MealsSummary>
        <MealList></MealList>
    </Fragment>;
}

export default Body;