import React, {useContext, useState} from 'react';
import Styles from './Header.module.css';
import HeaderCartButton from './partials/HeaderCartButton';

import Modal from '../UI/Modal';

import BasketContext from '../contex/basket-contex';

const Header = () => {


    const [modalStatus, setModalStatus] = useState(false);
    const ctx = useContext(BasketContext);

    const closeModal = () => {
        setModalStatus(false);
    }

    const openModal = () => {
        setModalStatus(true);
    }

    return (
        <React.Fragment>
            {modalStatus && <Modal closeModal={closeModal} basketItems={ctx.basket} />}
            <div className={Styles.header}>
                <div>
                    <h1>React Meals</h1>
                </div>
                <HeaderCartButton openModal={openModal}></HeaderCartButton>
            </div>
            <div className={Styles['main-image']}>
                <img src="meals.jpeg" alt="meals"></img>
            </div>
        </React.Fragment>
    );
}

export default Header;